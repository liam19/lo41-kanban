#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>
#include <sys/msg.h>
#include <pthread.h>
#include <string.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/syscall.h>

#define NB_SIGNAUX SIGRTMIN
#define NB_THREADS_MAX 100 //Nombre de postes à ne pas dépasser
#define NB_CONTENEURS NB_THREADS_MAX
#define SIGUSR2 12 //Contrainte liée à l'OS

// ID de la file de messages destinée au tableau de lancement
int fluxCarteMagnetique;

int nbrePiecesFinales;

int NB_THREADS;							 //Nombre de postes de travail total
pthread_t tabThreads[NB_THREADS_MAX][2]; //Fait la correspondance entre le n° du poste et son id de thread
pthread_t threadTableauDeLancement;

int nbPostesPrincipaux; //Nombre de postes principaux (longueur)

int conteneursDispo = NB_CONTENEURS;
int tableauConteneurs[NB_CONTENEURS];

pthread_mutex_t attributionConteneur;
pthread_cond_t attenteConteneur;
pthread_cond_t attenteAcces;
pthread_cond_t attenteLancement;

/**
 * @brief Structure des messages qui sera envoyé au tableau de lancement 
 * pour lui indiquer le poste à réveiller.
 * 
 */
struct kanban
{
	long type;
	char nomMatiere[256];
};

/**
 * @brief structure permettant de stocker les infos du poste/thread.
 */
struct poste
{
	int numeroPoste;
	int nbPiecesAProduire;
	int tempsFabrication;
	int conteneurEntrant;
	int conteneurSortant;
};

/**
 * @brief structure liant un poste principal à d'autres postes
 */
struct postePrincipal
{
	struct poste informations;
	struct poste *postesSecondaires;
	int length; //Nombre de postes secondaires associés au poste principal
};

//Tableau des données (struct postePrincipal) de tous les threads
struct postePrincipal tabPostesPrincipaux[NB_THREADS_MAX];

/**
 * @brief Structure des messages transitant entre les postes (conteneurs de pièces).
 * 
 */
struct conteneur
{
	long numeroPoste;
	int nombrePiecesProduites;
	char nomMatiere[256];
	int numeroConteneur;
};

void FermetureUsine();

/**
 * @brief initialise les informations de chaque poste principal
 * @param index numéro d'index dans le tableau de postes principaux
 */
void initialisationPoste(int index)
{
	int i;

	printf("-----------------\n");
	printf("Initialisation du Poste principal n.%d\n", index);

	printf("Combien de temps met-il pour produire une pièce à partir des éléments ?\n");
	scanf("%d", &tabPostesPrincipaux[index].informations.tempsFabrication);
	// tabPostesPrincipaux[index].informations.tempsFabrication = 2;
	printf("Combien de pièces doit-il produire ?\n");
	scanf("%d", &tabPostesPrincipaux[index].informations.nbPiecesAProduire);
	// tabPostesPrincipaux[index].informations.nbPiecesAProduire = 2;

	if ((tabPostesPrincipaux[index].informations.conteneurEntrant = msgget((key_t)index, IPC_CREAT | IPC_EXCL | 0600)) <= 0)
	{
		perror("Probleme lors de la création de la file de messages...\n");
		exit(1);
	}
}

/**
 * @brief lance l'initialisation des postes de l'usine 
 * fait appel à l'initialiseur pour chaque poste
 */
void initialisationPostes()
{
	int i, j;
	nbPostesPrincipaux = 0;
	NB_THREADS = 1;
	tabPostesPrincipaux[0].informations.numeroPoste = 0;

	printf("----------------------------------------------------\n");
	printf("Bienvenue dans le gestionnaire des postes de travail\n");
	printf("----------------------------------------------------\n\n");

	printf("Combien de pièces finales devons-nous produire ?\n");
	scanf("%d", &tabPostesPrincipaux[0].informations.nbPiecesAProduire);
	// tabPostesPrincipaux[0].informations.nbPiecesAProduire = 2;
	nbrePiecesFinales = tabPostesPrincipaux[0].informations.nbPiecesAProduire;

	if (tabPostesPrincipaux[0].informations.nbPiecesAProduire < 1)
	{
		printf("Le nombre de pièces à produire doit être positif\nArrêt.\n");
		exit(1);
	}

	printf("-----------------------------------------------------\n");
	printf("Début de l'initialisation de l'usine :\n");
	sleep(1);
	printf("Commençons à partir du Poste final\n");
	sleep(1);
	printf("Combien de Postes principaux sont associés au poste final ?\n");
	scanf("%d", &nbPostesPrincipaux);
	// nbPostesPrincipaux = 2;
	while (nbPostesPrincipaux > 5 || nbPostesPrincipaux < 1)
	{
		printf("\nOn vous conseillera un nombre de Postes principaux positif et petit (<5)\n");
		printf("Combien de temps met le Poste final pour produire 1 pièce s'il a tous les éléments ?\n");
		scanf("%d", &tabPostesPrincipaux[0].informations.tempsFabrication);
	}
	printf("Combien de temps met le Poste final pour produire 1 pièce s'il a tous les éléments ?\n");
	scanf("%d", &tabPostesPrincipaux[0].informations.tempsFabrication);
	// tabPostesPrincipaux[0].informations.tempsFabrication = 5;

	printf("Les postes suivants sont reliés au Poste final :\n");
	for (i = 1; i < nbPostesPrincipaux + 1; i++)
	{
		tabPostesPrincipaux[i].informations.numeroPoste = i;
		tabPostesPrincipaux[i].length = 0;
		printf("Poste %d\n", i);
		NB_THREADS++;
	}

	if ((tabPostesPrincipaux[0].informations.conteneurEntrant = msgget((key_t)0, IPC_CREAT | IPC_EXCL | 0600)) == -1)
	{
		perror("Erreur lors de la création de la file de message pour le poste 0\n");
		exit(1);
	}
	sleep(1);

	printf("\nMaintenant nous allons procédé poste par poste.\n");

	//On initialise tous les postes principaux
	for (i = 1; i < nbPostesPrincipaux + 1; i++)
		initialisationPoste(i);

	//Liaison des files de messages
	tabPostesPrincipaux[1].informations.conteneurSortant = tabPostesPrincipaux[0].informations.conteneurEntrant;
	printf("Conteneur sortant du Poste %d mis en correspondance avec le conteneur entrant du Poste final\n", 1);
	// printf("%d\n", tabPostesPrincipaux[1].informations.conteneurEntrant);
	for (i = 2; i < nbPostesPrincipaux + 1; i++)
	{
		tabPostesPrincipaux[i].informations.conteneurSortant = tabPostesPrincipaux[i - 1].informations.conteneurEntrant;
		printf("Conteneur sortant du Poste %d mis en correspondance avec le conteneur entrant du Poste %d\n", i, i - 1);
		// printf("%d\n", tabPostesPrincipaux[i].informations.conteneurEntrant);
	}
}

/**
 * @brief initialise le tableau de données des postes (threads).
 * 
 */
void initialisationTabThread()
{
	int i, j;
	for (i = 0; i < nbPostesPrincipaux; i++)
	{
		tabThreads[i][0] = i;
	}
}

/**
 * @brief Fonction permettant d'initialiser le flux de cartes magnétiques
 */
void initialisationFluxCartes()
{
	if ((fluxCarteMagnetique = msgget(123456, IPC_CREAT | 0600)) == -1)
	{
		perror("La file de messages n'a pas pu être crée.\n");
		exit(1);
	}
}

/**
 * @brief Fonction permettant de chercher un conteneur disponible, 
 * de le réserver et de retourner le numéro du conteneur 
 * correspondant au poste ayant appelé la fonction.
 * 
 * @return int : le numéro du conteneur
 */
int hommeFlux_attribuerConteneur()
{
	// On entre en zone critique
	pthread_mutex_lock(&attributionConteneur);

	int i, num_conteneur;

	if (conteneursDispo == 0)
		// On met le thread en attente jusqu'à ce qu'un conteneur se libère
		pthread_cond_wait(&attenteConteneur, &attributionConteneur);

	// Dans le cas où au moins un conteneur est dispo,
	// on parcourt le tableau des conteneurs et on prend le premier disponible.
	// printf("L'Homme-flux attribue un conteneur\n");
	for (i = 0; i < NB_CONTENEURS; i++)
	{
		if (tableauConteneurs[i] == 0) // Dans ce cas, le conteneur est disponible
		{
			num_conteneur = i;
			tableauConteneurs[i] = 1; // On considère que le conteneur n'est plus disponible
			i = NB_CONTENEURS;
		}
	}

	conteneursDispo--;
	// On sort de la zone critique
	pthread_mutex_unlock(&attributionConteneur);
	return num_conteneur;
}

/**
 * @brief Fonction permettant libérer un conteneur à partir de son numéro.
 * 
 * @param num : numéro du conteneur à libérer
 */
void hommeFlux_rendreConteneur(int num)
{
	// On entre en section critique
	pthread_mutex_lock(&attributionConteneur);

	// printf("L'Homme-flux rend le conteneur\n");

	tableauConteneurs[num] = 0;
	conteneursDispo++;
	pthread_cond_signal(&attenteConteneur);

	//On sort de la section critique
	pthread_mutex_unlock(&attributionConteneur);
}

/**
 * @brief Fonction permettant de sebloquer jusqu'à réception d'un SIGUSR2.
 */
void attendreSignal()
{
	// On définit un masque pour la réception de SIGUSR2
	sigset_t ens;
	sigfillset(&ens);
	sigdelset(&ens, SIGUSR2);

	sigsuspend(&ens);
}

/**
 * @brief Fonction permettant de récupérer un conteneur provenant de la file de messages indiquée par msgid.
 */
void recupererConteneurPlein(struct conteneur *cont, long numPoste, int msgid)
{
	msgrcv(msgid, cont, sizeof(struct conteneur), numPoste, 0);
}

/**
 * @brief Fonction complétant les informations d'un conteneur.
 */
void produire(struct conteneur *cont, long numPoste, int numConteneur, int nbrePieces)
{
	cont->numeroPoste = numPoste;
	cont->numeroConteneur = numConteneur;
	cont->nombrePiecesProduites = nbrePieces;
}

/**
 * @brief Fonction permettant d'envoyer un message (conteneur) dans la file de messages adéquate (identifiée par msgid).
 */
void envoyerConteneur(struct conteneur *cont, int msgid)
{
	msgsnd(msgid, cont, sizeof(struct conteneur), IPC_NOWAIT);
}

/**
 * @brief Fonction permettant de transférer un message (Kanban) dans la file de messages fluxCarteMagnetique.
 */
void envoyerKanban(int numPoste)
{
	// Préparation et envoi de la carte Kanban dans la boîte aux lettres
	struct kanban msg;
	msg.type = numPoste;
	strcpy(msg.nomMatiere, "VIDE");
	msgsnd(fluxCarteMagnetique, &msg, sizeof(struct kanban), 0);
}

/**
 * @brief Fonction appelée lors de la création des Threads "postes de travail".  
 * Simule le fonctionnement d'un poste de travail.
 */
void *posteTravail(void *arg)
{
	struct postePrincipal *donneesPoste = (struct postePrincipal *)arg;
	int numeroPoste = donneesPoste->informations.numeroPoste;
	int conteneurId;
	int i;
	struct conteneur conteneurPlein;
	struct conteneur conteneurARemplir;

	printf("On entre dans le poste %d\n", numeroPoste);

	// Début du process
	while (nbrePiecesFinales > 0)
	{
		if (numeroPoste > 0)
		{
			if (i == 0)
			{
				// Si on est au premier tour, tous les postes hormi le poste final créent un stock 
				produire(&conteneurARemplir, numeroPoste, conteneurId, donneesPoste->informations.nbPiecesAProduire);
				envoyerConteneur(&conteneurARemplir, donneesPoste->informations.conteneurSortant);
				printf("Poste %d : création d'un stock initial\n", numeroPoste);
				++i;
			}

			// On attend d'être réveillé par SIGUSR2
			printf("Poste %d : attente du signal...\n", numeroPoste);
			attendreSignal();

			// Dans le cas où les postes se bloquent et que poste final se termine
			if (nbrePiecesFinales == 0)
				break;
		}

		printf("Poste %d : Démarrage de la production.\n", numeroPoste);

		// On récupère un conteneur vide
		conteneurId = hommeFlux_attribuerConteneur();
		printf("Poste %d : récupération d'un conteneur vide n°%d\n", numeroPoste, conteneurId);

		// Si poste en bout de chaîne
		if (numeroPoste == nbPostesPrincipaux)
		{
			// On récupère un conteneur plein provenant du stock (infini)
			produire(&conteneurARemplir, numeroPoste, conteneurId, donneesPoste->informations.nbPiecesAProduire);
			printf("Poste %d : récupération d'un conteneur plein provenant du stock de matière première\n", numeroPoste);
		}
		else
		{
			// On envoie une carte kanban au tableau de lancement
			envoyerKanban(numeroPoste + 1);
			printf("Poste %d : envoi d'un kanban au poste %d\n", numeroPoste, numeroPoste + 1);

			// Récupération d'un conteneur plein provenant du poste en amont
			recupererConteneurPlein(&conteneurPlein, numeroPoste + 1, donneesPoste->informations.conteneurEntrant);
			printf("Poste %d : récupération d'un conteneur plein provenant du poste %d\n", numeroPoste, numeroPoste + 1);
		}

		// Pour tous les postes sauf le poste final
		if (numeroPoste > 0)
		{
			// On produit le nombre de pièces nécessaires
			for (i = 0; i < donneesPoste->informations.nbPiecesAProduire; i++)
			{
				printf("Poste %d : production d'une pièce... (%d secondes)\n", numeroPoste, donneesPoste->informations.tempsFabrication);
				sleep(donneesPoste->informations.tempsFabrication);
			}
			// On remplit le conteneur vide
			produire(&conteneurARemplir, numeroPoste, conteneurId, donneesPoste->informations.nbPiecesAProduire);
			printf("Poste %d : production terminée !\n", numeroPoste);

			// On envoie le conteneur
			envoyerConteneur(&conteneurARemplir, donneesPoste->informations.conteneurSortant);
			printf("Poste %d : envoi du conteneur fraichement rempli au poste %d\n", numeroPoste, numeroPoste - 1);
		}
		else
		{
			printf("Poste %d : une pièce produite !\n", numeroPoste);
			nbrePiecesFinales--;
			printf("Il reste %d pièce(s) finale(s) à produire\n", nbrePiecesFinales);
		}
	}

	if (numeroPoste == 0)
	{
		printf("\nPoste Final : production terminée !\n");
	}
	else
	{
		printf("Poste %d : production terminée !\n", numeroPoste);
	}

	pthread_exit(NULL);
}

/**
 * @brief La fonction se divise en deux parties :
	Partie 1 : Initialisation et lancement
		Il attend de recevoir un message via l'Homme Flux (fluxCarteMagnetique) 
		de tous les postes de travail (threads) avant d'envoyer un signal au poste final
		pour lui dire de commencer à produire.

	Partie 2 : Production
		Les threads sont parcourus et dès qu'il reçoit un message de l'Homme Flux 
		en provenance d'un poste i qui indique qu'il faut réveille le poste au niveau n-1 
		par rapport à lui, il lui envoie donc un signal
 * 
 * @param donnee 
 * @return void* 
 */
void *tableauDeLancement(void *arg)
{

	int i;
	struct kanban tampon;

	// On attend que tous les postes s'initialisent
	sleep(2);

	printf("On entre dans le tableau de lancement\n");
	fflush(stdout);

	while (nbrePiecesFinales > 0)
	{
		// On cherche toutes les cartes kanban provenant de tous les postes
		for (i = 0; i < nbPostesPrincipaux + 1; i++)
		{
			// Si une carte est reçue, on envoie un signal de réveil au poste en amont
			if (msgrcv(fluxCarteMagnetique, &tampon, sizeof(struct kanban), i, IPC_NOWAIT) > 0)
			{
				printf("Tableau de lancement : envoi d'un signal au poste %ld\n", tampon.type);
				pthread_kill(tabThreads[(int)tampon.type][1], SIGUSR2);
			}
		}
		sleep(3);
	}

	// On réveille tous les postes pour qu'ils se terminent
	for (i = 0; i < nbPostesPrincipaux + 1; i++)
	{
		pthread_kill(tabThreads[i][1], SIGUSR2);
	}

	printf("\nFin du tableau de lancement !\n");
	pthread_exit(NULL);
}

void initialisationThreads()
{
	int i;
	void *retour;

	// Création des Threads correspondant aux postes de travail
	for (i = 0; i < nbPostesPrincipaux + 1; i++)
	{
		if (pthread_create(&tabThreads[i][1], 0, posteTravail, (void *)&tabPostesPrincipaux[i]) != 0)
		{
			perror("Erreur Creation thread\n");
			exit(-1);
		}
	}

	// Création du Thread correspondant au Tableau de Lancement
	pthread_create(&threadTableauDeLancement, 0, tableauDeLancement, NULL);

	sleep(1);

	// Attente de terminaison des Threads
	for (i = 0; i < nbPostesPrincipaux + 1; i++)
	{
		if (pthread_join(tabThreads[i][1], &retour) != 0)
		{
			perror("Erreur attente du thread\n");
			exit(-1);
		}
		// printf("Retour : %d\n", (int *)retour);
	}

	FermetureUsine();

	pthread_join(threadTableauDeLancement, NULL);
}

/**
 * @brief La fonction termine tous les threads précédemment créés et supprime les files de messages
 * reliant les postes 2 à 2 ainsi la file de messages fluxCarteMagnetique.
 * 
 */
void FermetureUsine()
{
	int i;
	printf("\nFermeture de l'usine !\n");

	for (i = 0; i < NB_THREADS; i++)
	{
		pthread_cancel(tabThreads[i][1]);
	}

	pthread_cancel(threadTableauDeLancement);

	for (i = 0; i < nbPostesPrincipaux + 1; i++)
	{
		if (msgctl(tabPostesPrincipaux[i].informations.conteneurEntrant, IPC_RMID, 0) == -1)
		{
			perror("Problème de suppression de la file de messages\n");
		}
		printf("Poste %d fermé\n", i);
	}
	pthread_kill(threadTableauDeLancement, SIGINT);
	if (msgctl(fluxCarteMagnetique, IPC_RMID, 0) == -1)
		perror("Problème de suppression de la file de messages\n");
	exit(1);
}

/**
 * @brief Fonction permettant d'ignorer le signal SIGUSR2 pour le main
 * Le masque ne fonctionne pas pour le main ...
 */
void tmp()
{
}

/**
 * @brief réalise les actions suivantes :
 * 
 * - Appel de la fonction récursive d'initialisation des postes et des threads dans 
 * le tableau tabDonneesThread
 * 
 * - Initialisation du tableau de lancement
 */
void process()
{
	signal(SIGUSR2, tmp);
	sigset_t ens;
	sigemptyset(&ens);
	sigaddset(&ens, SIGUSR2); // On bloque SIGUSR2 au niveau du processus ?
	sigprocmask(SIG_SETMASK, &ens, NULL);

	signal(SIGINT, FermetureUsine);

	initialisationPostes();	//On crée l'architecture
	initialisationTabThread(); //On instancie la table des threads
	initialisationFluxCartes();

	pthread_mutex_init(&attributionConteneur, NULL);

	sleep(1);
	printf("------------------------\n");
	printf("Début de la production\n");
	printf("------------------------\n\n");

	initialisationThreads();

	msgctl(fluxCarteMagnetique, IPC_RMID, 0);
}

/**
 * @brief lance le programme - l'usine pour le kanban
 */
int main(void)
{
	process();

	return EXIT_SUCCESS;
}